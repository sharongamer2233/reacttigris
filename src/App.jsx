import React, { useState } from 'react';
import Articale from './assets/Articale';


function App() {
  const [likes1, setLikes1] = useState(0); 
  const [likes2, setLikes2] = useState(0);
  const [likes3, setLikes3] = useState(0); 
  const [likes4, setLikes4] = useState(0);
  const [likes5, setLikes5] = useState(0); 
  const [likes6, setLikes6] = useState(0);


  const incrementLikes1 = () => {
    setLikes1(likes1 + 1);
  };

  
  const incrementLikes2 = () => {
    setLikes2(likes2 + 1);
  };

  const incrementLikes3 = () => {
    setLikes3(likes3 + 1);
  };

  const incrementLikes4 = () => {
    setLikes4(likes4 + 1);
  };

  const incrementLikes5 = () => {
    setLikes5(likes5 + 1);
  };

  const incrementLikes6 = () => {
    setLikes6(likes6 + 1);
  };

  return (
    <>
      <div className="awesome" style={{ border: "1px solid red" }}>
        <label htmlFor="name">Enter your name: </label>
        <input type="text" id="name" />
      </div>
      <p>Enter your HTML here</p>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Document</title>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"
      />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="" />
      <link
        href="https://fonts.googleapis.com/css2?family=Lora:wght@600&family=Poppins:ital@0;1&display=swap"
        rel="stylesheet"
      />
      <link rel="stylesheet" href="./index.css" />
      <header>
        <div className="container">
          <span id="logo">Tigris</span>
          <button id="menubutton">
            <span className="material-symbols-outlined">pets</span>
          </button>
          <nav>
            <ul id="navlist">
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
      <main>
        <section>
          <div className="container">
            <h1>Welcome to Tigris Tiger Reserve</h1>
            <p>
              Tigris Tiger Reserve, nestled amidst lush greenery, is a sanctuary for
              the majestic big cats. Spanning vast landscapes, it harbors diverse
              flora and fauna, including endangered species. Visitors immerse in
              nature's tranquility while contributing to conservation efforts.
              Through eco-friendly tourism, Tigris safeguards habitats, ensuring the
              survival of its iconic inhabitants for generations to come.
            </p>
            <div className="buttonholder">
              <a className="button buttonprimary" href="#">
                Book a safari
              </a>
              <a className="button buttonsecondary" href="#">
                Contact us
              </a>
            </div>
          </div>
        </section>
        <section id="sectionabout">
          <div className="container">
            <h2>About Tigris Tiger Reserve</h2>
            <p>
              Discover the rich tapestry of wildlife conservation at Tigris Tiger
              Reserve. Established with a commitment to preserving biodiversity, our
              sanctuary provides a haven for endangered species, including the
              majestic Bengal tiger. Through sustainable practices and community
              engagement, we strive to safeguard habitats and promote coexistence
              between humans and wildlife in this pristine natural environment.
            </p>
          </div>
        </section>
        <section>
          <div className="container">
            <h2>Roars and insights</h2>
            <div className="bloglist">

              {/* articles go's here */}
              <Articale incrementLikes={incrementLikes1} likes={likes1} />
              <Articale incrementLikes={incrementLikes2} likes={likes2} />
              <Articale incrementLikes={incrementLikes3} likes={likes3} />
              <Articale incrementLikes={incrementLikes4} likes={likes4} />
              <Articale incrementLikes={incrementLikes5} likes={likes5} />
              <Articale incrementLikes={incrementLikes6} likes={likes6} />

            </div>
          </div>
        </section>
      </main>
      <footer />
    </>
  );
}

export default App;