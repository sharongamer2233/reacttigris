import React from 'react';

function Articale({ incrementLikes, likes}) {
  return (
    <>
      <article className="blog">
            <img src="https://w0.peakpx.com/wallpaper/444/66/HD-wallpaper-tiger-animal-bengal-big-cat-nature-wild-thumbnail.jpg" alt="" />
            <h3>Conserving the magestic big cats</h3>
            <p>
              "Delve into the noble endeavor of conserving the majestic big
              cats, exploring their habitat, threats, and the crucial role of
              conservation efforts in preserving their existence."
            </p>
            <div>
               <button onClick={incrementLikes}>like</button>
               <span id='likeBtn'><span class="material-symbols-outlined">thumb_up</span>{likes}</span>
            </div>
          </article>
    </>
  );
}

export default Articale;